import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from '../components/App';
import Page404 from '../components/Page404';

const AppRouter = () => (
  <BrowserRouter>
    <Fragment>
      <Switch>
        <Route exact path="/" component={App}></Route>
        <Route component={Page404}></Route>
      </Switch>
    </Fragment>
  </BrowserRouter>
);

export default AppRouter;