import React from 'react';

// 10 cities with the highest temperatures
class CitiesHighestTemp extends React.Component {
  state = {
    cityIndex: "0",
    dateIndex: "0",
    highestTemp: ""
  }

  handleSelectedCity = (e) => {
    const cityIndex = e.target.value;
    this.setState(() => ({ cityIndex }));
  }

  handleSelectedDate = (e) => {
    const dateIndex = e.target.value;
    this.setState(() => ({ dateIndex }));
  }

  handleHighestTemp = (e) => {
    e.preventDefault();
    let highestTemp = 0;

    const { cityIndex, dateIndex } = this.state;
    
    // eslint-disable-next-line
    this.props.cities[cityIndex].temperatures[dateIndex].temps.map((temp) => {
      if(temp.temp > highestTemp) {
        highestTemp = temp.temp;
      }
    });

    this.setState({ highestTemp });
  }

  render() {
    const {cities} = this.props;
    return (
      <div>
        <h3>Highest temp of a city in a certain day</h3>
        <form onSubmit={this.handleHighestTemp}>
          <select onChange={this.handleSelectedCity}>
            {
              cities.map((city, index) => (
                <option key={city.name+index} value={index}>{city.name}</option>
              ))
            }
          </select>
          <select onChange={this.handleSelectedDate}>
            {
              cities[this.state.cityIndex].temperatures.map((temp, index) => (
                <option key={temp.date+index} value={index}>{temp.date}</option>
              ))
            }
          </select>
          <input type="submit" value="Check" />
        </form>
        {
         this.state.highestTemp && (
            <p>Highest temp: {this.state.highestTemp}º Celsius</p>
          )
        }
      </div>
    ); 
  }
}

export default CitiesHighestTemp;