import React from 'react'

class HighestTempXDays extends React.Component {
  state = {
    days: 0,
    highestTemp: 0 
  }
  // Highest temperature of the last "X" days
  onDaysChange = (e) => {
    const days = e.target.value;
    const { cities } = this.props;
    let highestTemp = 0;

    // eslint-disable-next-line
    cities.map((city) => {
      let TempsIndex = city.temperatures.length - 1;
      
      for (let i = 0; i < days; i++) {
        // eslint-disable-next-line no-loop-func
        // eslint-disable-next-line
        city.temperatures[TempsIndex].temps.map((temp) => {
          if(temp.temp > highestTemp) {
            highestTemp = temp.temp;
          }
        });
        TempsIndex --;
      }
    })
    this.setState({ days, highestTemp });
  }

  render() {
    return (
      <div>
        <h3>HighestTemperature of the last X days</h3>
        <input type="text" onChange={this.onDaysChange}/>
        <p>HighestTemp (in the last {this.state.days} days) is: <b>{this.state.highestTemp}</b>º Celsius</p>
      </div>
    );
  }
}

export default HighestTempXDays;