import React from 'react';

const Temperature = ({handlePersistentTemp, handleSelectedDate, handleSelectedCity, handleSelectedTime, showTemp, cityIndex, dateIndex, cities}) => (
  <div>
    <h3>Temperature of a city</h3>

    <form onSubmit={handlePersistentTemp}>
      <select onChange={handleSelectedCity}>
        {
          cities.map((city, index) => (
            <option key={city.name+index} value={index}>{city.name}</option>
          ))
        }
      </select>
      <select onChange={handleSelectedDate}>
        {
          cities[cityIndex].temperatures.map((temp, index) => (
            <option key={temp.date+index} value={index}>{temp.date}</option>
          ))
        }
      </select>
      <select onChange={handleSelectedTime}>
        {
          cities[cityIndex].temperatures[dateIndex].temps.map((time, index) => (
            <option key={time.time+index} value={index}>{time.time}</option>
          ))
        }
      </select>
      <input type="submit" value="Search" />
    </form>
    {
      showTemp.status && (
        <div>
          <p>Temperature: { showTemp.temp }º Celsius</p>
        </div>
      )
    }
  </div>
);

export default Temperature;