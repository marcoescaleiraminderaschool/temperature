import React from 'react';

const AddTemperature = ({cities, onAddTemperature}) => (
  <div>
    <h3>Add a temperature to a city</h3>
    <form onSubmit={onAddTemperature}>
      <select name="country">
        {
          cities.map((city, index) => (
            <option key={city.name+index} value={index}>{city.name}</option>
          ))
        }
      </select>
      <input type="text" name="temp" placeholder="10"/>
      <input type="submit" value="Add" />
    </form>
  </div>
)

export default AddTemperature;