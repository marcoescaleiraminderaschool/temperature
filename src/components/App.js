import React, { Component } from 'react';
// import uuid from 'uuid';
import Moment from 'moment';
import Temperature from './Temperature';
import AddTemperature from './AddTemperature';
import CityInfo from './CityInfo';
import HighestTempXDays from './HighestTempXDays';
import CitiesHighestTemp from './CitiesHighestTemp';

import Amsterdam from './cities/Amsterdam';
import Barcelona from './cities/Barcelona';
import Liverpool from './cities/Liverpool';
import London from './cities/London';
import Madrid from './cities/Madrid';
import Matosinhos from './cities/Matosinhos';
import Moscow from './cities/Moscow';
import Paris from './cities/Paris';
import Porto from './cities/Porto';
import Southampton from './cities/Southampton';
import Toronto from './cities/Toronto';

class App extends Component {
  state = {
    cities: [
      Amsterdam,
      Barcelona,
      Liverpool,
      London,
      Madrid,
      Matosinhos,
      Moscow,
      Paris,
      Porto,
      Southampton,
      Toronto
    ],
    showTemp: {
      status: false,
      temp: ''
    },
    cityIndex: "0",
    dateIndex: "0",
    timeIndex: "0"
  }
  
  handleSelectedCity = (e) => {
    const cityIndex = e.target.value;
    this.setState(() => ({ cityIndex }));
  }

  handleSelectedDate = (e) => {
    const dateIndex = e.target.value;
    this.setState(() => ({ dateIndex }));
  }

  handleSelectedTime = (e) => {
    const timeIndex = e.target.value;
    this.setState(() => ({ timeIndex }));
  }

  handlePersistentTemp = (e) => {
    e.preventDefault();

    const { cityIndex, dateIndex, timeIndex } = this.state;
    const temp = this.state.cities[cityIndex].temperatures[dateIndex].temps[timeIndex].temp;
    
    this.setState(() => ({ showTemp: {
      status: true,
      temp
    }}));
  }

  onAddTemperature = (e) => {
    e.preventDefault();
    const countryIndex = e.target.country.value;
    const date = Moment().format("DD/MM/YYYY");
    const time = Moment().format("hh:mm:ss");
    const temp = e.target.temp.value;
    let citiesUpdated = {}

    const addTemp = {
      date: date,
      temps: [
        {
          time: time,
          temp: temp
        }
      ] 
    }
    const { cities } = this.state
    // Verificar a data de hoje , se já existe (map)
    const tempsLength = cities[countryIndex].temperatures.length - 1;
    if (cities[countryIndex].temperatures[tempsLength].date !== date) {
       citiesUpdated = cities.map((city) => {
        if (city === cities[countryIndex]) {
            city.temperatures.push(addTemp);
        }
        return city;
      });
    } else {
      citiesUpdated = cities.map((city) => {
        if (city === cities[countryIndex]) {
          // const dateTemps = city.temperatures.filter((temp) => temp.date === date);
          
          city.temperatures[tempsLength].temps.push({
            time: time,
            temp: temp
          });
        }
        console.log(city);
        return city;
      });
    }
    
    this.setState(({ cities: citiesUpdated }));
  }

  render() {
    const { cityIndex, dateIndex } = this.state;

    return (
      <div>
        <h1>Home</h1>
        <hr />

        <Temperature 
          handlePersistentTemp={this.handlePersistentTemp}
          handleSelectedDate={this.handleSelectedDate}
          handleSelectedCity={this.handleSelectedCity}
          handleSelectedTime={this.handleSelectedTime}
          showTemp={this.state.showTemp}
          cityIndex={cityIndex}
          dateIndex={dateIndex}
          cities={this.state.cities}
        />
        <hr />
        <AddTemperature 
          cities={this.state.cities}
          onAddTemperature={this.onAddTemperature}
        />
        <hr />
        <CityInfo 
          cities={this.state.cities}
        />
        <hr />
        <HighestTempXDays 
          cities={this.state.cities}
        />
        <hr />
        <CitiesHighestTemp 
          cities={this.state.cities}
        />
      </div>
    );
  }
}

export default App;
