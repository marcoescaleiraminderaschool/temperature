import React from 'react';


class CityInfo extends React.Component {
  state = {
    cityIndex: "0"
  }

  handleCityInfo = (e) => {
    const cityIndex = e.target.value;
    this.setState(() => ({ cityIndex }));
  }

  render() {
    const { cities } = this.props;
    const { cityIndex } = this.state;
    return (
      <div>
        <h3>City information</h3>
        <select onChange={this.handleCityInfo}>
            {
              cities.map((city, index) => (
                <option key={city.name+index} value={index}>{city.name}</option>
              ))
            }
          </select>
          <p>Country: { cities[cityIndex].country }</p>
          <p>Latitude: { cities[cityIndex].latitude }</p>
          <p>Longitude: { cities[cityIndex].longitude }</p>
      </div>
    );
  }
}

export default CityInfo;