const Madrid = {
  name: 'Madrid',
  country: 'Spain',
  latitude: "41° 9' 0\" N",
  longitude: "8° 37' 0\" W",
  temperatures: [
    {
      date: '29/01/2019',
      temps: [
        {
          time: '11:00',
          temp: 13
        },
        {
          time: '18:00',
          temp: 22
        }
      ] 
    },
    {
      date: '30/01/2019',
      temps: [
        {
          time: '12:00',
          temp: 33
        }
      ] 
    },
    {
      date: '31/01/2019',
      temps: [
        {
          time: '10:00',
          temp: 35
        }
      ] 
    },
    {
      date: '01/02/2019',
      temps: [
        {
          time: '09:00',
          temp: 10
        },
        {
          time: '14:00',
          temp: 37
        }
      ] 
    },
    {
      date: '02/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 25
        },
        {
          time: '23:00',
          temp: 30
        }
      ] 
    },
    {
      date: '03/02/2019',
      temps: [
        {
          time: '16:00',
          temp: 40
        },
        {
          time: '21:00',
          temp: 22
        }
      ] 
    },
    {
      date: '04/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 33
        },
        {
          time: '23:00',
          temp: 12
        }
      ] 
    },
    {
      date: '05/02/2019',
      temps: [
        {
          time: '11:00',
          temp: 39
        },
        {
          time: '00:00',
          temp: 16
        }
      ] 
    },
    {
      date: '06/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 33
        },
        {
          time: '17:00',
          temp: 17
        }
      ] 
    },
    {
      date: '07/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 32
        },
        {
          time: '13:00',
          temp: 11
        }
      ] 
    }
  ]
};

export default Madrid;