const London = {
  name: 'London',
  country: 'England',
  latitude: "41° 9' 0\" N",
  longitude: "8° 37' 0\" W",
  temperatures: [
    {
      date: '29/01/2019',
      temps: [
        {
          time: '11:00',
          temp: 13
        },
        {
          time: '18:00',
          temp: 22
        }
      ] 
    },
    {
      date: '30/01/2019',
      temps: [
        {
          time: '12:00',
          temp: 33
        }
      ] 
    },
    {
      date: '31/01/2019',
      temps: [
        {
          time: '10:00',
          temp: 35
        }
      ] 
    },
    {
      date: '01/02/2019',
      temps: [
        {
          time: '09:00',
          temp: 80
        },
        {
          time: '14:00',
          temp: 100
        }
      ] 
    },
    {
      date: '02/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 25
        },
        {
          time: '23:00',
          temp: 30
        }
      ] 
    },
    {
      date: '03/02/2019',
      temps: [
        {
          time: '16:00',
          temp: 40
        },
        {
          time: '21:00',
          temp: 22
        }
      ] 
    },
    {
      date: '04/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 38
        },
        {
          time: '23:00',
          temp: 50
        }
      ] 
    },
    {
      date: '05/02/2019',
      temps: [
        {
          time: '11:00',
          temp: 39
        },
        {
          time: '00:00',
          temp: 40
        }
      ] 
    },
    {
      date: '06/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 33
        },
        {
          time: '17:00',
          temp: 17
        }
      ] 
    },
    {
      date: '07/02/2019',
      temps: [
        {
          time: '12:00',
          temp: 32
        },
        {
          time: '13:00',
          temp: 90
        }
      ] 
    }
  ]
};

export default London;