# Start the App

## Requirements:
 - Node
 - Yarn
### `brew install node`
### `brew install yarn`

## To open the project run:

### 1 - `yarn`

### 2 - `yarn run build`

### 3 - `yarn global add serve`

### 4 - `serve -s build`